<?php

namespace Quadra\Comuni;

class Regione extends \Innomatic\Dataaccess\DataAccessObject {
    /**
     * Innomatic container.
     *
     * @var \Innomatic\Core\InnomaticContainer
     */
    protected $container;

    /**
     * Regione internal identifier number.
     *
     * @var integer
     */
    protected $regioneId;

    /**
     * Item name.
     *
     * @var string
     * @access protected
     */
    protected $name;

    /**
     * Item description.
     *
     * @var string
     */
    protected $description;

    /**
     * Class constructor.
     *
     * @param number $id Optional item identifier number.
     */
    public function __construct($id = 0)
    {
        $this->container  = \Innomatic\Core\InnomaticContainer::instance('\Innomatic\Core\InnomaticContainer');
        parent::__construct($this->container->getCurrentTenant()->getDataAccess());

        $this->regioneId = $id;

        // If an item id has been given during object creation,
        // fetch the item data from the database.
        //
        if ($id != 0) {
            $this->getRegione($id);
        }

    }

    /**
     * Adds a new regione to the database and initializes the current object.
     *
     * @param string $name Regione name.
     * @param string $description Regione description.
     * @access public
     * @return void
     */
    public function addRegione($name, $description)
    {
        // Get a sequence number for the new item.
        //
        $id = $this->dataAccess->getNextSequenceValue('regioni_table_id_seq');

        // Format a string for SQL.
        //
        $nameValue = $this->dataAccess->formatText($name);

        $descriptionValue = $this->dataAccess->formatText($description);

        // Insert the new item in the database.
        // We use the parent class (DataAccessObject) update() method.
        //
        $result = $this->update(
            'INSERT INTO regioni_table ' .
            '(id, name, description) ' .
            'VALUES (' .
            $id . ',' .
            $nameValue . ',' .
            $descriptionValue . ')'
        );

        // If the query has been successful, initialize the object attributes.
        //
        if ($result) {
            $this->regioneId    = $id;
            $this->name         = $name;
            $this->description  = $description;
        }

        return $result;
    }

    /**
     * Retrieves all the regioni from the database.
     *
     * @return \Innomatic\Dataaccess\DataAccessResult Items.
     */
    public function findAllRegioni()
    {
        return $this->retrieve('SELECT * FROM regioni_table');
    }

    /**
     * Retrieves the content of a regione and set the object attributes.
     *
     * @param integer $id
     */
    public function getRegione($id)
    {
        $regione = $this->retrieve("SELECT * FROM regioni_table WHERE id=$id");

        if ($regione->getNumberRows() == 1) {
            // This is a string, no need to process it.
            //
            $this->regioneId    = $id;
            $this->name         = $regione->getFields('name');
            $this->description  = $regione->getFields('description');

        }

        return $this;
    }

    /**
     * Returns regione identifier number.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->regioneId;
    }

    /**
     * Returns regione name.
     *
     * @access public
     * @return name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns regione description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the regione name.
     *
     * @param string $name regione name
     * @return \Quadra\Comuni\Regione The regione object itself.
     */
    public function setName($name)
    {
        // Set the object attribute.
        //
        $this->name = $name;

        return $this;
    }

    /**
     * Sets the regione description.
     *
     * @param string $description regione description.
     * @return \Quadra\Comuni\Regione The regione object itself.
     */
    public function setDescription($description)
    {
        // Set the object attribute.
        //
        $this->description = $description;

        return $this;
    }

    /**
     * This method stores the object in the database.
     *
     * It must be called after changing one or more object attributes.
     *
     * This method checks if the item object is valid.
     *
     * @access public
     * @return \Quadra\Comuni\Regione The regione object itself.
     */
    public function storeRegione()
    {
        if ($this->regioneId != 0) {
            // Prepare the values.

            // Format a string for SQL.
            //
            $name        = $this->dataAccess->formatText($this->name);

            $description = $this->dataAccess->formatText($this->description);

            // Update the database row.
            //
            $this->update(
                'UPDATE regioni_table '.
                'SET '.
                "name           = $name, ".
                "description    = $description ".
                "WHERE id = $this->cimiteroId}"
            );
        }

        return $this;
    }

    /**
     * Disable the current item.
     *
     * @return void
     */
    public function disableCimitero()
    {
        if ($this->cimiteroId != 0) {
            $this->update(
                "UPDATE cimiteri_table " .
                "SET " .
                "status = 0 " .
                "WHERE id = {$this->cimiteroId}"
            );

            // Empty item attributes.
            //
            $this->name = '';
            $this->description = '';
        }
    }

    /**
     * Deletes the current item from the database.
     *
     * @return void
     */
    public function deleteCimitero()
    {
        if ($this->cimiteroId != 0) {
            // Remove the item row from the database.
            //
            $this->update(
                'DELETE FROM cimiteri_table '.
                'WHERE id='.$this->cimiteroId
            );

            // Empty the item attributes.
            //
            $this->name = '';
            $this->description = '';
        }

    }

    /**
     * Retrieves all items asc ordered from the database.
     *
     * @return \Innomatic\Dataaccess\DataAccessResult Items.
     */
    public function findAllOrderedCimiteri()
    {
        return $this->retrieve('SELECT * FROM cimiteri_table WHERE status = 1 ORDER BY name ASC');
    }

    public function findFirstCimitero()
    {
        return $this->retrieve('SELECT * FROM cimiteri_table WHERE status = 1 ORDER BY name ASC LIMIT 1');
    }

}
